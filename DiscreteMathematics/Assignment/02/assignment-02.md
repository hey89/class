# Assignment 02

> Write a source code in C programming for computing the summation and the subtraction by a command line

## Parameters

- A series of a symbol for the calculation and a positive integer
- Calculation symbol is either '+' or '-'
- The maximum number of (symbol, number) pairs is 100

## Return Values

- Type : integer
- Value : result of the calculation based on the calculation symbols and the integers

## Examples

```console
$ program.exe + 10 - 20 + 35 - 27
```

## Print out

- The output of the computation
- Personal signature that can identify the writer in a unique way
- The output of the computation is followed by the personal signature

```console
$ program.exe + 10 - 20 + 35 - 27
-2
*********************
Byung-Woo Hong
Student ID : 12345678
*********************
```

## Submission 

- Source code [.c]
- git commit history [.pdf]
- Screen capture of the result for the print out with an example input [.png]

## File naming convention

- Source code : STUDENTID_ASSIGNMENT##.c [example: 20191234_02.c]
- git commit history : STUDENTID_ASSIGNMENT##.pdf [example: 20191234_02.pdf]
- Screen capture of an example output : STUDENTID_ASSIGNMENT##.png [example: 20191234_02.png]